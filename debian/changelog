python-jenkins (1.4.0-3) unstable; urgency=medium

  * Add allow-pkg_resources.parse_version-to-fail.patch (Closes: #1030412).

 -- Thomas Goirand <zigo@debian.org>  Tue, 14 Feb 2023 10:20:37 +0100

python-jenkins (1.4.0-2) unstable; urgency=medium

  * Fix upsteram URLs (Closes: #998851).

 -- Thomas Goirand <zigo@debian.org>  Tue, 28 Dec 2021 19:19:42 +0100

python-jenkins (1.4.0-1) unstable; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Blacklist test_get_view_jobs_raise_HTTPError() which is an obvious false
    positive error.

 -- Thomas Goirand <zigo@debian.org>  Tue, 31 Aug 2021 16:51:53 +0200

python-jenkins (0.4.16-2) unstable; urgency=medium

  [ Mattia Rizzolo ]
  * Remove Fathi Boudra <fabo@debian.org> from uploaders.  Closes: #879425
  * Mark pthon-jenkins-doc as Multi-Arch:foreign.
  * Bump Standards-Version to 4.1.4, no changes needed.

  [ Ondřej Nový ]
  * Running wrap-and-sort -bast
  * d/copyright: Use https protocol in Format field
  * Removing gbp.conf, not used anymore or should be specified in the
    developers dotfiles
  * d/control: Use team+openstack@tracker.debian.org as maintainer
  * Use 'python3 -m sphinx' instead of sphinx-build for building docs
  * Use debhelper-compat instead of debian/compat.

  [ Thomas Goirand ]
  * Remove useless overrides.
  * Switch to debhelper-compat 11.

 -- Thomas Goirand <zigo@debian.org>  Sat, 09 Jan 2021 21:40:41 +0100

python-jenkins (0.4.16-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Mon, 04 Jan 2021 17:08:14 +0100

python-jenkins (0.4.16-1) unstable; urgency=medium

  * Ran wrap-and-sort -bast.
  * Point VCS to Salsa.
  * New upstream release.
  * Kill Python 2 support.
  * Standards-Version is now 4.1.3.

 -- Thomas Goirand <zigo@debian.org>  Thu, 05 Apr 2018 14:08:10 +0000

python-jenkins (0.4.14-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Fixed homepage (https).
  * Fixed VCS URLs (https).
  * d/rules: Removed UPSTREAM_GIT, changed to default value
  * d/copyright: Changed source URL to https protocol

  [ Thomas Goirand ]
  * New VCS URLs.
  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 03 Aug 2017 20:55:52 +0000

python-jenkins (0.4.11-1) unstable; urgency=medium

  * New upstream release (Closes: #806719).
  * Added new (build-)depends: python{3,}-multi-key-dict.
  * Added python-testscenarios as build-depends.

 -- Thomas Goirand <zigo@debian.org>  Tue, 22 Dec 2015 08:49:17 +0000

python-jenkins (0.4.8-2) unstable; urgency=medium

  [ Fathi Boudra ]
  * Added myself as Uploader.

  [ Thomas Goirand ]
  * Added missiong python{3,}-setuptools build-depends (Closes: #802714).

 -- Thomas Goirand <zigo@debian.org>  Fri, 23 Oct 2015 07:37:07 +0000

python-jenkins (0.4.8-0.1) unstable; urgency=medium

  * Putting the package under the OpenStack PKG team, with agreement from Paul:
    - New VCS URLs.
    - Team as maintainer.
    - James, Paul and myself as uploaders.
    - Using git tag based packaging (fixed gbp.conf).
  * New upstream release.
  * Removed usless version from python-all build-depends.
  * Removed now useless X-Python-Version: >= 2.6.
  * Standards-Version is now 3.9.6.
  * Added python-pbr and dh-python as build-depends.
  * Fixed debian/copyright.
  * Added Python3 support.
  * Now running unit tests at build time.
  * Fixed debian/copyright as directed by the FTP masters.
  * debian/watch file now using github tag to avoid PyPi broken one.
  * Now generating the docs.

 -- Thomas Goirand <zigo@debian.org>  Mon, 24 Aug 2015 14:50:54 +0200

python-jenkins (0.2.1-0.1) unstable; urgency=medium

  * Non-maintainer upload.
  * New upstream release.
  * Merge changes from Ubuntu: force use of python distutils for build system
  * Add add-fixes-for-crumb-addition-to-post-requests.patch - stolen upstream

 -- Fathi Boudra <fabo@debian.org>  Sun, 11 May 2014 17:51:22 +0300

python-jenkins (0.2-2) unstable; urgency=low

  * debian/control:
    - We are an architecture-independent package so set Architecture: all
    - Drop XSBC-Original-Maintainer field
    - Add James Page to Maintainer and Paul Belange to Uploaders
  * debian/copyright:
    - Add missing BSD-3-Clause License text
  * debian/rules:
    - Update get-orig-source making it debian-policy (4.9) compliant
  * debian/source.lintian-overrides:
    - Removed

 -- Paul Belanger <paul.belanger@polybeacon.com>  Sat, 17 Nov 2012 13:57:41 -0500

python-jenkins (0.2-1) unstable; urgency=low

  * Initial release (Closes: #693268)

 -- Paul Belanger <paul.belanger@polybeacon.com>  Wed, 14 Nov 2012 17:39:03 -0500

python-jenkins (0.2-0ubuntu2) precise; urgency=low

  * Rebuild to drop python2.6 dependencies.

 -- Matthias Klose <doko@ubuntu.com>  Sat, 31 Dec 2011 02:10:45 +0000

python-jenkins (0.2-0ubuntu1) oneiric; urgency=low

  * New upstream release.

 -- James Page <james.page@ubuntu.com>  Wed, 10 Aug 2011 17:43:01 +0100

python-jenkins (0.1-0ubuntu1) oneiric; urgency=low

  * Initial release

 -- James Page <james.page@ubuntu.com>  Thu, 30 Jun 2011 16:41:07 +0100
